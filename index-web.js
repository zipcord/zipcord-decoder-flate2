/** Entry point for using decoder from a browser - adds setup function for
  initializing the WASM module */

import initWasmDecoder, {
  decodeBuffer, Flate2InflatorSink, Flate2InflatorTransformer
} from './decoder-pkg'


// TODO: Consider replacing this with a URL to a CDN like unpkg
const DEFAULT_WASM_SOURCE = 'decoder-pkg/index_bg.wasm'
let wasmInitPromise

/** Initializes WASM module if not already done.

  @param source - Source of WASM module. Passed from `makeFlate2Config`.
  @returns `Promise` that resolves when WASM module is ready
*/
function initFlate2(source) {
  if (!wasmInitPromise) {
    // First time calling, WASM still needs initialization
    // Cast return to Promise<void> as we do not need initWasmDecoder's
    // return value
    if (initWasmDecoder instanceof Function) {
      // Only run initWasmDecoder if defined, initialization only required when
      // using wasm-pack's web or no-modules targets
      wasmInitPromise = initWasmDecoder(source)
    } else {
      wasmInitPromise = Promise.resolve()
    }
  }

  return wasmInitPromise
}


/** Creates a setup function for DecoderConfig.

  @param wasmModule - Source of decoder-flate2's WASM module. Defaults to a
    relative URL of `decoder-pkg/index_bg.wasm`. You can also give it
    an `ArrayBuffer` containing the code, a `Request` or `Response` object that
    loads the module, or a `Promise` that resolves to a valid value. If you are
    familiar with wasm-pack this is the value passed to the init() function
    provided by the web/no-modules targets.
*/
export function createFlate2Setup(source) {
  return () => initFlate2(source).then(
    () => ({
      createSink: (uncompressedSize) => {
        const sink = new Flate2InflatorSink(uncompressedSize)
        const promise = sink.getBufferPromise()
        return [sink, promise]
      },
      createTransformer: (uncompressedSize) => new Flate2InflatorTransformer(uncompressedSize),
      decodeBuffer
    })
  )
}
