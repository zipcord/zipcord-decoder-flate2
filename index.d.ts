import type { InitInput } from './decoder-pkg'


/** Creates a setup function for DecoderConfig */
export declare function createFlate2Setup(source?: InitInput | Promise<InitInput>): () => Promise<{
  createSink: (uncompressedSize: number) => [sink: UnderlyingSink<Uint8Array>, promise: Promise<Uint8Array>],
  createTransformer: (uncompressedSize: number) => Transformer<Uint8Array, Uint8Array>,
  decodeBuffer: (inputBuffer: Uint8Array, uncompressedSize: number) => Promise<Uint8Array>,
}>
