WASM-based decoder for [Zipcord](https://zipcord.gitlab.io/zipcord/) written in
Rust using [flate2](https://crates.io/crates/flate2) to decompress files.

## Usage

This decoder requires a WASM module to function. It exports a function named
`createFlate2Setup` that takes the URL of the WASM module as an argument and
returns a new function for use with
[`ZipcordOptions.decoderSetup`](https://zipcord.gitlab.io/zipcord/interfaces/zipcordoptions.html#decodersetup).
If you have installed this package from NPM you can find the WASM module at
`node_modules/zipcord-decoder-flate2/decoder-pkg/index_bg.wasm`. If built
from source it will simply be `decoder-pkg/index_bg.wasm` from the root of the
repository.

The WASM module is only needed for browser targets - when used in Node loading
of the module from the filesystem is done automatically. However, there is
little reason to use this decoder in Node, as Zipcord will use [native
zlib](https://nodejs.org/api/zlib.html) in that case.

```typescript
import { Zipcord } from "zipcord";
import { createFlate2Setup } from "zipcord-decoder-flate2";

const archive = await ZipcordRaw.open(buffer, {
    decoderSetup: createFlate2Setup('https://example.com/decoder-pkg/index_bg.wasm')
});

// Zipcord will wait for the decoder to finish loading before extracting any
// deflate-compressed archive entries. If you want to ensure the module is
// loaded before attempting to read a compressed file, you can check that the
// decoder promise has successfully resolved before extracting. This promise
// will also reject if there's a problem loading the module, allowing you to
// check for errors initializing the decoder.
await archive.decoder;

const wasmBuffer = loadWasmFromSomewhereAsArrayBuffer();
ZipcordRaw.open(buffer, { decoderSetup: createFlate2Setup(wasmBuffer) });
```

Besides an URL, `createFlate2Setup` can also take an `ArrayBuffer` containing
the module, a `Request` or `Response` object that loads the module, or a
`Promise` that resolves to a valid value. If you are familiar with
[wasm-pack](https://github.com/rustwasm/wasm-pack) this is the value passed to
the `init()` function provided by the web/no-modules targets.

```typescript
const wasmBuffer = loadWasmFromSomewhereAsArrayBuffer();
ZipcordRaw.open(buffer, { decoderSetup: createFlate2Setup(wasmBuffer) });
```


## Limitiations

Due to WASM's 32-bit architecture, this decoder cannot be used to decompress
files larger than approximately 4GB. This limitation only applies when
extracting to a buffer, there is no limit when decoding as a stream.


## Building

Building `zipcord-decoder-flate2` requires [Rust](https://www.rust-lang.org/)
and [wasm-pack](https://github.com/rustwasm/wasm-pack). Once installed, you can
use `npm run build` to build and package it. You may also substitute
`npm run build:web` or `npm run build:node` if you only want to build the web
or NodeJS targets.


## Status

This decoder is experimental and currently not recommended for general use. It
is slower and has a larger payload than the Javascript-based
[Pako](https://gitlab.com/zipcord/zipcord-decoder-fflate) and
[fflate](https://gitlab.com/zipcord/zipcord-decoder-fflate) decoders. This may
change in the future due to improvements in the decoder and in WASM itself, but
at this time should probably not be used in production. See the [benchmark
page](https://zipcord.gitlab.io/zipcord-demo/bench.html) to compare performance.


## Credits/license

Zipcord is copyright © David Powell and is licenced under MIT. See LICENSE.txt
for full text of license.

Built using [flate2](https://github.com/rust-lang/flate2-rs), a DEFLATE
compression/decompression library for Rust.
