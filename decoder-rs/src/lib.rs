use std::io::Write;

use flate2::FlushDecompress;
use js_sys::{Function, Promise, Uint8Array};
use wasm_bindgen::prelude::*;


type Result<T = ()> = std::result::Result<T, JsValue>;

// Bindings for TransformStreamDefaultController. Could be removed in the
// future if/when they get added to web-sys.
#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_name = TransformStreamDefaultController)]
    pub type TransformStreamDefaultController;

    #[wasm_bindgen(method, catch)]
    fn enqueue(this: &TransformStreamDefaultController, out_chunk: &Uint8Array) -> Result<()>;

    #[wasm_bindgen(method)]
    fn error(this: &TransformStreamDefaultController, reason: &JsValue);
}

/// Writer for DeflateDecoder that enqueues written chunks to stream. Must set
/// controller with the active stream controller before writing chunks.
struct ChunkQueue {
    pub controller: TransformStreamDefaultController
}

impl ChunkQueue {
    fn new() -> Self {
        Self {
            controller: TransformStreamDefaultController {
                obj: JsValue::NULL
            }
        }
    }
}

impl Write for ChunkQueue {
    fn write(&mut self, bytes: &[u8]) -> std::io::Result<usize> {
        if let Err(reason) = self.controller.enqueue(&Uint8Array::from(bytes)) {
            self.controller.error(&reason);
        }
        Ok(bytes.len())
    }

    fn flush(&mut self) -> std::io::Result<()> {
        Ok(())
    }
}

/// Create a string containing an error message
fn make_error_string(desc: &str, reason: String) -> String {
    // Don't use format! to avoid bloating build size
    let mut error_message = desc.to_owned();
    error_message.push_str(": ");
    error_message.push_str(&reason);
    error_message
}

/// Javascript WritableStream sink that decodes deflate-compressed data using
/// the flate2 crate.
#[wasm_bindgen]
pub struct Flate2InflatorSink {
    decoder: flate2::write::DeflateDecoder<Vec<u8>>,
    promise: Promise,
    promise_resolve: Function,
}

#[wasm_bindgen]
impl Flate2InflatorSink {
    #[wasm_bindgen(constructor)]
    pub fn new(uncompressed_size: usize) -> Self {
        let mut promise_resolve = None;
        let promise = Promise::new(&mut |res: Function, _rej: Function| {
            promise_resolve = Some(res);
        });
        // Can safely unwrap, value was set in closure above
        let promise_resolve = promise_resolve.unwrap();

        // usize on WASM is 4 bytes, which means this decoder is cannot output
        // entries greater than 4GB (archives larger than that can still be
        // read, this limitation is only for uncompressed file entries). This
        // is a limitation of WASM itself and cannot be fixed. WASM may offer a
        // 64-bit version in the future that would lift this limit but as this
        // moment Memory64 is nothing more than hopes and dreams.
        //
        // Ok, it could be fixed by passing decoded chunks out to JS and
        // merging them before resolving, but I'm not going to do that. If
        // you're decoding 4GB+ files in a web browser you're going to have a
        // bad time no matter the technical limitations. You should probably be
        // using the stream methods for enormous files when you can anyways.
        let writer: Vec<u8> = Vec::with_capacity(uncompressed_size);
        Self {
            decoder: flate2::write::DeflateDecoder::new(writer),
            promise,
            promise_resolve,
        }
    }

    /// Write a chunk from a ReadableStream to flate2 inflator
    pub fn write(&mut self, chunk: &[u8]) -> Result {
        self.decoder.write_all(chunk)
            .map_err(|e| make_error_string("Could not write chunk", e.to_string()))?;
        Ok(())
    }

    /// ReadableStream output has finished
    pub fn close(&mut self) -> Result {
        let buffer = self.decoder.reset(Vec::new())
            .map_err(|e| make_error_string("Could not reset decoder", e.to_string()))?;

        // Copy output buffer to JS. In the future we should be able to do this
        // without copying. Maybe.
        let js_array = Uint8Array::from(buffer.as_slice());
        self.promise_resolve.call1(&JsValue::UNDEFINED, &js_array)?;

        Ok(())
    }

    /// Returns a Promise that resolves with the output buffer when stream is
    /// closed. Used by JS wrapper to create DecoderConfig.
    #[wasm_bindgen(js_name = getBufferPromise)]
    pub fn get_buffer_promise(&self) -> Promise {
        self.promise.clone()
    }
}

/// Javascript TransformStream Transformer that decodes deflate-compressed data
/// using the flate2 crate.
#[wasm_bindgen]
pub struct Flate2InflatorTransformer {
    decoder: flate2::write::DeflateDecoder<ChunkQueue>,
}

#[wasm_bindgen]
impl Flate2InflatorTransformer {
    #[wasm_bindgen(constructor)]
    pub fn new() -> Self {
        let writer = ChunkQueue::new();
        Self {
            decoder: flate2::write::DeflateDecoder::new(writer),
        }
    }

    /// Take a compressed chunk and queue any number of decompressed chunks
    pub fn transform(&mut self, chunk: &[u8], controller: TransformStreamDefaultController) -> Result {
        self.decoder.get_mut().controller = controller;
        // Output chunks queued by ChunkQueue
        self.decoder.write_all(chunk)
            .map_err(|e| make_error_string("Could not transform chunk", e.to_string()) )?;

        Ok(())
    }

    /// TransformStream output has finished
    pub fn flush(&mut self, controller: TransformStreamDefaultController) -> Result {
        self.decoder.get_mut().controller = controller;
        self.decoder.try_finish()
            .map_err(|e| make_error_string("Could not flush Transformer", e.to_string()) )?;

        Ok(())
    }
}

/// Decodes a deflated buffer to an output buffer
#[wasm_bindgen(js_name = decodeBuffer)]
pub fn decode_buffer(input_buffer: &[u8], uncompressed_size: usize) -> Result<Uint8Array> {
    let mut decoder = flate2::Decompress::new(false);
    let mut output_buffer = Vec::with_capacity(uncompressed_size);
    decoder.decompress_vec(&input_buffer, &mut output_buffer, FlushDecompress::Finish)
        .map_err(|e| make_error_string("Could not decode buffer", e.to_string()) )?;

    Ok(Uint8Array::from(output_buffer.as_slice()))
}
