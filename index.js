/** Entry point for using decoder from Node - no setup required */

import {
  decodeBuffer, Flate2InflatorSink, Flate2InflatorTransformer
} from './decoder-pkg-node/index.js'


/** Creates a setup function for DecoderConfig */
export function createFlate2Setup() {
  return () => Promise.resolve({
    createSink: (uncompressedSize) => {
      const sink = new Flate2InflatorSink(uncompressedSize)
      const promise = sink.getBufferPromise()
      return [sink, promise]
    },
    createTransformer: (uncompressedSize) => new Flate2InflatorTransformer(uncompressedSize),
    decodeBuffer
  })
}
